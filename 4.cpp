#include <iostream>
#include <thread>
#include <cstring>
#include <string>
#include <sstream>
#include <map>
#include <mysql/mysql.h>
using namespace std;

int main(){

    MYSQL mysql;
    mysql_init(&mysql);
    const char* host = "127.0.0.1";
    const char* user = "root";
    const char* pass = "123456";
    const char* db = "laoxiaoketang";

    //连接超时
    int to = 3;
    int re = mysql_options(&mysql,MYSQL_OPT_CONNECT_TIMEOUT,&to);
    if(re != 0){
        std::cerr<<"mysql_options failed!"<<mysql_error(&mysql)<<std::endl;
    }

    //自动重连
    int reconnect = 3;
    mysql_options(&mysql,MYSQL_OPT_RECONNECT,&reconnect);

    //连接（阻塞的）
    if(!mysql_real_connect(&mysql,host,user,pass,db,3306,0,0)){
        std::cerr<<"mysql connect failed!"<<mysql_error(&mysql)<<std::endl;
    }else{
        std::cout<<"mysql connect "<<host<<" success!"<<std::endl;
    }

    std::string sql = "";

    //1.创建表
    sql =R"(CREATE tABLE IF NOT EXISTS `t_image`(
        `id` int AUTO_INCREMENT,
        `name` varchar(1024),
        `path` varchar(1024),
        `size` int,
        PRIMARY KEY(`id`)
    ))";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    sql = R"(truncate t_image)";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    int count = 0;

for(int i=0;i<10000;i++){
    //2.插入数据
    //sql = R"(insert `t_image` (`name`,`path`,`size`) values('test.jpg','/mnt/data',10240))";
    stringstream ss;
    ss << R"(insert `t_image` (`name`,`path`,`size`) values('image_)";
    ss <<i<< R"(.jpg','/mnt/data',10240))";
    sql = ss.str();
    cout<<sql<<endl;
    re = mysql_query(&mysql,sql.c_str());
    int count = mysql_affected_rows(&mysql);
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }else{
        cout<<"mysql_affected_rows:"<<count<<" id="<<mysql_insert_id(&mysql)<<endl;
    }
    
}

    //3.修改数据
    sql = R"(update t_image set `name` = "test3.png",`size`="4000" where id = 2)";
    cout<<sql<<endl;
    re = mysql_query(&mysql,sql.c_str());
    count = mysql_affected_rows(&mysql);
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }else{
        cout<<"mysql_affected_rows:"<<count<<" id="<<mysql_insert_id(&mysql)<<endl;
    }

    map<std::string,std::string> kv;
    kv.insert(make_pair("name",R"("image_upload001.png")"));
    kv.insert(make_pair("size",R"("5000")"));
    kv.insert(make_pair("path",R"("/usr/local")"));
    std::string where = R"(where `id` = 100)";
    std::string tmp = "";
    for(auto ptr = kv.begin();ptr!= kv.end();ptr++){
        tmp += "`";
        tmp += ptr->first;
        tmp += "`=";
        tmp += ptr->second;
        tmp += ",";
    }
    tmp += "id=id";
    sql = "update t_image set ";
    sql += tmp;
    sql += " ";
    sql += where;
    std::cout<<sql<<endl;
    re = mysql_query(&mysql,sql.c_str());
    count = mysql_affected_rows(&mysql);
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }else{
        cout<<"mysql_affected_rows:"<<count<<endl;
    }

    //4.删除数据
    sql = R"(delete from t_image)";
    cout<<sql<<endl;
    re = mysql_query(&mysql,sql.c_str());
    count = mysql_affected_rows(&mysql);
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }else{
        cout<<"mysql_affected_rows:"<<count<<endl;
    }
    

    mysql_close(&mysql);
    mysql_library_end();
    std::cout<<"Mysql 8.0 API"<<endl;
    return 0;
}