#include <iostream>
#include <thread>
#include <cstring>
#include <string>
#include <sstream>
#include <map>
#include <mysql/mysql.h>
using namespace std;

int main(){

    MYSQL mysql;
    mysql_init(&mysql);
    const char* host = "127.0.0.1";
    const char* user = "root";
    const char* pass = "123456";
    const char* db = "laoxiaoketang";

    //连接超时
    int to = 3;
    int re = mysql_options(&mysql,MYSQL_OPT_CONNECT_TIMEOUT,&to);
    if(re != 0){
        std::cerr<<"mysql_options failed!"<<mysql_error(&mysql)<<std::endl;
    }

    //自动重连
    int reconnect = 3;
    mysql_options(&mysql,MYSQL_OPT_RECONNECT,&reconnect);

    //连接（阻塞的）
    if(!mysql_real_connect(&mysql,host,user,pass,db,3306,0,CLIENT_MULTI_STATEMENTS)){
        std::cerr<<"mysql connect failed!"<<mysql_error(&mysql)<<std::endl;
    }else{
        std::cout<<"mysql connect "<<host<<" success!"<<std::endl;
    }

    std::string sql = "";

    //1.创建表
    sql =R"(CREATE tABLE IF NOT EXISTS `t_video`(
        `id` int AUTO_INCREMENT,
        `name` varchar(1024),
        `path` varchar(2046),
        `size` int,
        PRIMARY KEY(`id`)
    ) ENGINE=InnoDB;)";//设置支持事务回滚的InnoDB引擎

    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    sql = R"(truncate t_video)";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    //---------------------------------事务------------------------------

    //1.开始事务
    //START TRANSACTION
    sql = "START TRANSACTION";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    //2.设置为手动提交
    sql = "set autocommit=0";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    //3.sql语句
    for(int i=0;i<3;i++){
        sql = R"(insert into t_video (name) values('test three'))";
        re = mysql_query(&mysql,sql.c_str());
        if(re != 0){
            cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
        }
    }

    //4.回滚ROLLBACK MYISAM 不支持
    sql = "ROLLBACK";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

        //3.sql语句
    for(int i=0;i<100;i++){
        sql = R"(insert into t_video (name) values('test three'))";
        re = mysql_query(&mysql,sql.c_str());
        if(re != 0){
            cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
        }
    }
    
    //5.commit
    sql = "COMMIT";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }

    //6.恢复自动提交
    sql = "set autocommit=1";
    re = mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }


    sql = "select count(*) from t_video";
    re =mysql_query(&mysql,sql.c_str());
    if(re != 0){
        cout<<"mysql_query failed!"<<mysql_error(&mysql)<<std::endl;
    }
    MYSQL_RES *res =  mysql_store_result(&mysql);
    if(res){
            MYSQL_ROW row = mysql_fetch_row(res);
            if(row){
                cout<<"t_video count(*)="<<row[0]<<endl;
            }
    }

    mysql_close(&mysql);
    mysql_library_end();
    std::cout<<"Mysql 8.0 API"<<endl;
    return 0;
}